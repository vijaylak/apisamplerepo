package com.sample.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jayway.restassured.response.Response;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Vijay Lakshmikanthan on 1/30/2018.
 * This is a Utility class that enables the conversion of the response object received from the service call to
 * be converted into the response object of the actual service using the beans.
 * Conversion is necessary to validate all the fields of REST-ASSURED response object without peeking into each field.
 * Conversion also enables easy Assertion.
 */
public class JsonParser {
    private static final Logger logger = Logger.getLogger("StoLog");


    public static String ObjectToString(Object object) {
        Gson gson = new GsonBuilder().create();
        String jsonString = gson.toJson(object);

        return jsonString;
    }

    public static Object StringToObject(String string, Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Object obj = mapper.readValue(string, object.getClass());
        return obj;
    }

    public static Object StringToObject(String string, Class cls) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Object obj = mapper.readValue(string, cls);
        return obj;
    }


    public static String printValueObject(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        // mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        String result = mapper.writeValueAsString(object);

        return result;
    }

    public static PersonErrorResponse convertGetPResponseToPersonErrorResponse(Response response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        PersonErrorResponse resp = mapper.readValue(response.asString(), PersonErrorResponse.class);
        return resp;
    }

    public static TokenErrorResponse convertGetTResponseToTokenErrorResponse(Response response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        TokenErrorResponse resp = mapper.readValue(response.asString(), TokenErrorResponse.class);
        return resp;
    }

    public static EApplyErrorResponse convertGetEResponseToEApplyErrorResponse(Response response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        EApplyErrorResponse resp = mapper.readValue(response.asString(), EApplyErrorResponse.class);
        return resp;
    }

    //C:\Users\590256\Documents\SoapuiAuto\MyInfoService_BDD\resources\InputJson\G1612352N.json
    public static PrefillSuccessResponse convertFileToResponseObject(String authID) throws IOException {

        BufferedReader br = new BufferedReader(new FileReader("resources/InputJson/" + authID + ".json"));
        PrefillSuccessResponse response = new Gson().fromJson(br, PrefillSuccessResponse.class);
        return response;
    }

    public static PrefillSuccessResponse convertHttpResponseToPrefill(String response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        PrefillSuccessResponse response1 = mapper.readValue(response, PrefillSuccessResponse.class);

        return response1;
    }

}
