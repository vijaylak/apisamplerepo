package com.sample.helpers;


import com.sample.variables.MyInfoSvcConstants;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Test;

import java.io.IOException;

import static com.jayway.restassured.RestAssured.given;

/**
 * Created by Vijay Lakshmikanthan on 1/29/2018.
 */
public class RestPostHelper {
    private static Logger logger = Logger.getLogger("StoLog");

    /**
     * This Class is intended to GET or POST Requests to a WebService based on different parameters
     * Framework used is REST-ASSURED
     * There is flexibility to used both Http client/REST-assured to make the calls.
     *
     */

    public static String getPrefillMyInfoData(String authID, String env) throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = null;
        if (env.equalsIgnoreCase("E1")) {
            logger.info("URL to Hit:" + MyInfoSvcConstants.MyInfoE1URL + authID);
            request = new HttpGet(MyInfoSvcConstants.MyInfoE1URL + authID);
        } else if (env.equalsIgnoreCase("E2")) {
            logger.info("URL to Hit:" + MyInfoSvcConstants.MyInfoE2URL + authID);
            request = new HttpGet(MyInfoSvcConstants.MyInfoE2URL + authID);
        } else {
            throw new SkipException("Invalid Environment Specified!!");
        }
        logger.info("Setting Headers!!");
        request.addHeader(MyInfoSvcConstants.C_ID, MyInfoSvcConstants.ClientId);
        request.addHeader(MyInfoSvcConstants.MSG_ID, MyInfoSvcConstants.mType);
        request.addHeader(MyInfoSvcConstants.D_Provider, MyInfoSvcConstants.dataP);
        request.addHeader(MyInfoSvcConstants.redirect_uri, MyInfoSvcConstants.uri);
        logger.info("Posting the request!!");
        HttpResponse response = client.execute(request);


        HttpEntity entity1 = response.getEntity();
        String respERA = EntityUtils.toString(entity1);
        logger.trace("Response received :" + respERA);
        Assert.assertEquals(response.getStatusLine().getStatusCode(), 200, "Request Failed!");
        return respERA;

    }

    public static String getPrefillFailureMessage(String header, String authID, String env) throws IOException {
        logger.trace("hitting the url without the header :" + header);

        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = null;
        if (env.equalsIgnoreCase("E1")) {
            logger.info("URL to Hit:" + MyInfoSvcConstants.MyInfoE1URL + authID);
            request = new HttpGet(MyInfoSvcConstants.MyInfoE1URL + authID);
        } else if (env.equalsIgnoreCase("E2")) {
            logger.info("URL to Hit:" + MyInfoSvcConstants.MyInfoE2URL + authID);
            request = new HttpGet(MyInfoSvcConstants.MyInfoE2URL + authID);
        } else {
            throw new SkipException("Invalid Environment Specified!!");
        }

        logger.info("Setting Headers!!");
        if (header.equalsIgnoreCase("ClientID")) {
            request.addHeader(MyInfoSvcConstants.MSG_ID, MyInfoSvcConstants.mType);
            request.addHeader(MyInfoSvcConstants.D_Provider, MyInfoSvcConstants.dataP);
            request.addHeader(MyInfoSvcConstants.redirect_uri, MyInfoSvcConstants.uri);
        } else if (header.equalsIgnoreCase("MSGTYPEID")) {
            request.addHeader(MyInfoSvcConstants.C_ID, MyInfoSvcConstants.ClientId);
            request.addHeader(MyInfoSvcConstants.D_Provider, MyInfoSvcConstants.dataP);
            request.addHeader(MyInfoSvcConstants.redirect_uri, MyInfoSvcConstants.uri);
        } else if (header.equalsIgnoreCase("DATAPROVIDER")) {
            request.addHeader(MyInfoSvcConstants.C_ID, MyInfoSvcConstants.ClientId);
            request.addHeader(MyInfoSvcConstants.MSG_ID, MyInfoSvcConstants.mType);
            request.addHeader(MyInfoSvcConstants.redirect_uri, MyInfoSvcConstants.uri);
        } else if (header.equalsIgnoreCase("uri")) {
            request.addHeader(MyInfoSvcConstants.C_ID, MyInfoSvcConstants.ClientId);
            request.addHeader(MyInfoSvcConstants.MSG_ID, MyInfoSvcConstants.mType);
            request.addHeader(MyInfoSvcConstants.D_Provider, MyInfoSvcConstants.dataP);
        }

        logger.info("Posting the request!!");
        HttpResponse response = client.execute(request);
        HttpEntity entity1 = response.getEntity();
        String respERA = EntityUtils.toString(entity1);

        logger.trace("Response :" + respERA);
        Assert.assertEquals(response.getStatusLine().getStatusCode(), 400, "Wrong HTTP error code received!");

        return respERA;
    }


    public static String getPrefillFailureNullMessage(String header, String authID, String env) throws IOException {
        logger.trace("hitting the url without the header :" + header);

        String cId = MyInfoSvcConstants.ClientId;
        String mId = MyInfoSvcConstants.mType;
        String dPro = MyInfoSvcConstants.dataP;
        String rURI = MyInfoSvcConstants.uri;

        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = null;
        if (env.equalsIgnoreCase("E1")) {
            logger.info("URL to Hit:" + MyInfoSvcConstants.MyInfoE1URL + authID);
            request = new HttpGet(MyInfoSvcConstants.MyInfoE1URL + authID);
        } else if (env.equalsIgnoreCase("E2")) {
            logger.info("URL to Hit:" + MyInfoSvcConstants.MyInfoE2URL + authID);
            request = new HttpGet(MyInfoSvcConstants.MyInfoE2URL + authID);
        } else {
            throw new SkipException("Invalid Environment Specified!!");
        }


        if (header.equalsIgnoreCase("ClientID")) {
            cId = null;
        } else if (header.equalsIgnoreCase("MSGTYPEID")) {
            mId = null;
        } else if (header.equalsIgnoreCase("DATAPROVIDER")) {
            dPro = null;
        } else if (header.equalsIgnoreCase("uri")) {
            rURI = null;
        }
        logger.info("Setting Headers!!");
        request.addHeader(MyInfoSvcConstants.C_ID, cId);
        request.addHeader(MyInfoSvcConstants.MSG_ID, mId);
        request.addHeader(MyInfoSvcConstants.D_Provider, dPro);
        request.addHeader(MyInfoSvcConstants.redirect_uri, rURI);

        logger.info("Posting the request!!");
        HttpResponse response = client.execute(request);
        HttpEntity entity1 = response.getEntity();
        String respERA = EntityUtils.toString(entity1);

        logger.trace("Response :" + respERA);
        Assert.assertEquals(response.getStatusLine().getStatusCode(), 400, "Wrong HTTP error code received!");

        return respERA;
    }

    public static String getPrefillFailureNegativeScenarioCall(String authCode, String env, int httpcode) throws IOException {
        logger.trace("400 Scenario Call");
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = null;
        if (env.equalsIgnoreCase("E1")) {
            logger.info("URL to Hit:" + MyInfoSvcConstants.MyInfoE1URL + authCode);
            request = new HttpGet(MyInfoSvcConstants.MyInfoE1URL + authCode);
        } else if (env.equalsIgnoreCase("E2")) {
            logger.info("URL to Hit:" + MyInfoSvcConstants.MyInfoE2URL + authCode);
            request = new HttpGet(MyInfoSvcConstants.MyInfoE2URL + authCode);
        } else {
            throw new SkipException("Invalid Environment Specified!!");
        }

        logger.info("Setting Headers!!");
        request.addHeader(MyInfoSvcConstants.C_ID, MyInfoSvcConstants.ClientId);
        request.addHeader(MyInfoSvcConstants.MSG_ID, MyInfoSvcConstants.mType);
        request.addHeader(MyInfoSvcConstants.D_Provider, MyInfoSvcConstants.dataP);
        request.addHeader(MyInfoSvcConstants.redirect_uri, MyInfoSvcConstants.uri);
        logger.info("Posting the request!!");

        logger.info("Posting the request!!");
        HttpResponse response = client.execute(request);
        HttpEntity entity1 = response.getEntity();
        String respERA = EntityUtils.toString(entity1);

        logger.trace("Response :" + respERA);
        Assert.assertEquals(response.getStatusLine().getStatusCode(),httpcode,"Wrong Error Code Populated");


        return respERA;
    }

    public static String getPrefillMyInfoDataSuccess(String authID) throws IOException {
        logger.trace("hitting the url without the header :" + authID);
        logger.info("URL To HIT :   " + MyInfoSvcConstants.MyInfoE1URL + authID);
        Response response = given()
                            .contentType(MyInfoSvcConstants.JSON)
                            .header(MyInfoSvcConstants.C_ID, MyInfoSvcConstants.ClientId)
                            .header(MyInfoSvcConstants.MSG_ID, MyInfoSvcConstants.mType)
                            .header(MyInfoSvcConstants.D_Provider, MyInfoSvcConstants.dataP)
                            .header(MyInfoSvcConstants.redirect_uri, MyInfoSvcConstants.uri)
                            .when()
                            .get(MyInfoSvcConstants.MyInfoE1URL + authID)
                            .then()
                            .extract()
                            .response();

        logger.trace("Response Received: " + response.asString());
        logger.trace("Response Received: " + response.getBody().asString());

        logger.trace("Validating the Status Code:" + response.getStatusCode());
        Assert.assertEquals(response.getStatusCode(), 200, "INVALID ERROR MESSAGE RECEIVED");

        return response.asString();
    }

    public static String getPrefillFailureScenarioMessage(String header, String authID, String env) throws IOException {
        logger.trace("hitting the url without the header :" + header);
        RequestSpecBuilder builder = new RequestSpecBuilder();
        RequestSpecification spec = null;

        logger.info("Setting Headers!!");
        if (header.equalsIgnoreCase("ClientID")) {
            logger.info("Omitting "+header);
            builder.addHeader(MyInfoSvcConstants.MSG_ID, MyInfoSvcConstants.mType);
            builder.addHeader(MyInfoSvcConstants.D_Provider, MyInfoSvcConstants.dataP);
            builder.addHeader(MyInfoSvcConstants.redirect_uri, MyInfoSvcConstants.uri);
            spec = builder.build();
        } else if (header.equalsIgnoreCase("MSGTYPEID")) {
            logger.info("Omitting "+header);
            builder.addHeader(MyInfoSvcConstants.C_ID, MyInfoSvcConstants.ClientId);
            builder.addHeader(MyInfoSvcConstants.D_Provider, MyInfoSvcConstants.dataP);
            builder.addHeader(MyInfoSvcConstants.redirect_uri, MyInfoSvcConstants.uri);
            spec = builder.build();
        } else if (header.equalsIgnoreCase("DATAPROVIDER")) {
            logger.info("Omitting "+header);
            builder.addHeader(MyInfoSvcConstants.C_ID, MyInfoSvcConstants.ClientId);
            builder.addHeader(MyInfoSvcConstants.MSG_ID, MyInfoSvcConstants.mType);
            builder.addHeader(MyInfoSvcConstants.redirect_uri, MyInfoSvcConstants.uri);
            spec = builder.build();
        } else if (header.equalsIgnoreCase("uri")) {
            logger.info("Omitting "+header);
            builder.addHeader(MyInfoSvcConstants.C_ID, MyInfoSvcConstants.ClientId);
            builder.addHeader(MyInfoSvcConstants.MSG_ID, MyInfoSvcConstants.mType);
            builder.addHeader(MyInfoSvcConstants.D_Provider, MyInfoSvcConstants.dataP);
            spec = builder.build();
        }

        logger.info("Posting the request!!");
        Response response = given()
                            .contentType(MyInfoSvcConstants.JSON)
                            .spec(spec)
                            .when()
                            .get(MyInfoSvcConstants.MyInfoE1URL + authID)
                            .then()
                            .extract()
                            .response();

        logger.trace("Response :" + response.asString());
        logger.trace("Validating the Status Code:"+response.getStatusCode());
        Assert.assertEquals(response.getStatusCode(), 400, "Wrong HTTP error code received!");

        return response.asString();
    }

    public static String getPrefillFailureNullScenarioMessage(String header, String authID, String env) throws IOException {
        logger.trace("hitting the url without the header :" + header);

        String cId = MyInfoSvcConstants.ClientId;
        String mId = MyInfoSvcConstants.mType;
        String dPro = MyInfoSvcConstants.dataP;
        String rURI = MyInfoSvcConstants.uri;



        if (header.equalsIgnoreCase("ClientID")) {
            cId = null;
        } else if (header.equalsIgnoreCase("MSGTYPEID")) {
            mId = null;
        } else if (header.equalsIgnoreCase("DATAPROVIDER")) {
            dPro = null;
        } else if (header.equalsIgnoreCase("uri")) {
            rURI = null;
        }
        logger.info("Setting Headers!!");
        logger.info("Posting the request!!");
        Response response = given()
                            .contentType(MyInfoSvcConstants.JSON)
                            .header(MyInfoSvcConstants.C_ID, cId)
                            .header(MyInfoSvcConstants.MSG_ID, mId)
                            .header(MyInfoSvcConstants.D_Provider, dPro)
                            .header(MyInfoSvcConstants.redirect_uri, rURI)
                .when()
                .get(MyInfoSvcConstants.MyInfoE1URL + authID)
                .then()
                            .extract()
                            .response();


        logger.trace("Response :" + response.asString());
        Assert.assertEquals(response.getStatusCode(), 400, "Wrong HTTP error code received!");

        return response.asString();
    }


    public static String getPrefillFailureNegativeCall(String authCode, String env, int httpcode) throws IOException {
        logger.trace("hitting the url without the header :" + authCode);
        logger.info("URL To HIT :   " + MyInfoSvcConstants.MyInfoE1URL + authCode);
        Response response = given()
                            .contentType(MyInfoSvcConstants.JSON)
                            .header(MyInfoSvcConstants.C_ID, MyInfoSvcConstants.ClientId)
                            .header(MyInfoSvcConstants.MSG_ID, MyInfoSvcConstants.mType)
                            .header(MyInfoSvcConstants.D_Provider, MyInfoSvcConstants.dataP)
                            .header(MyInfoSvcConstants.redirect_uri, MyInfoSvcConstants.uri)
                .when()
                .get(MyInfoSvcConstants.MyInfoE1URL + authCode)
                .then()
                            .extract()
                            .response();

        logger.trace("Response Received: " + response.asString());
        logger.trace("Response Received: " + response.getBody().asString());

        logger.trace("Validating the Status Code:" + response.getStatusCode());
        logger.trace("Http Code Expected:   "+httpcode);

        Assert.assertEquals(response.getStatusCode(), httpcode, "INVALID ERROR MESSAGE RECEIVED");

        return response.asString();
    }

    @Test
    public void sample2() throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet("https://weprefillsvc-dev.sample.com/acquisition/digital/v1/applications/prefill_info/S9912375C");
        request.addHeader(MyInfoSvcConstants.C_ID, "");
        request.addHeader(MyInfoSvcConstants.MSG_ID, MyInfoSvcConstants.mType);
        request.addHeader(MyInfoSvcConstants.D_Provider, MyInfoSvcConstants.dataP);
        request.addHeader(MyInfoSvcConstants.redirect_uri, MyInfoSvcConstants.uri);
        HttpResponse response = client.execute(request);
        HttpEntity entity1 = response.getEntity();
        String respERA = EntityUtils.toString(entity1);

        logger.trace("Response :" + respERA);
    }


    @Test
    public void Sample() {
        logger.info("Trial 1");
        Response response = given()
                .contentType(MyInfoSvcConstants.JSON)
                .header(MyInfoSvcConstants.C_ID, MyInfoSvcConstants.ClientId)
                .header(MyInfoSvcConstants.MSG_ID, MyInfoSvcConstants.mType)
                .header(MyInfoSvcConstants.D_Provider, MyInfoSvcConstants.dataP)
                .header(MyInfoSvcConstants.redirect_uri, MyInfoSvcConstants.uri)
                .when()
                .get("https://vweprefillsvc-dev.sample.com/acquisition/digital/v1/applications/prefill_info/S9912375C")
                .then()
                .extract()
                .response();

        logger.trace("Response Received: " + response.asString());
        logger.trace("Response Received: " + response.asString());

        logger.trace("res:" + response.getBody().asString());
        logger.info("response status code:" + response.getStatusCode());

    }

}
