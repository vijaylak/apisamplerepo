package com.sample.helpers;

import org.apache.log4j.Logger;
import org.testng.Assert;

/**
 * Created by Vijay Lakshmikanthan on 1/29/2018.
 */

public class RestResponseValidator {

/*
*
     * This class is to validate the response received from the Web Service Requests
     * Mapping the REST-Assured Response Object to Bean has ensured easy assertion at both Object level and field level

*/

    private static Logger logger = Logger.getLogger("StoLog");


    /*
    *
         * Validator method for response validation

    */
    public static void validateErrorResponse(ErrorMessage errorMessage, String expectedError, String header) {
        logger.trace("Validating the Error Message response!");
        logger.trace("Validating the error code ");
        if (header.equalsIgnoreCase("ClientID")) {
            logger.info("Validating client header missing scenario");
            Assert.assertEquals(errorMessage.getCode(), "1404", "Wrong error code received in message body for Client Header Missing");

            Assert.assertEquals(errorMessage.getUser_message(), expectedError, "Error Message in Error response body is wrong for Missing " + header);

        } else if (header.equalsIgnoreCase("MSGTYPEID")) {
            logger.info("Validating MSGTYPEID header missing scenario");
            Assert.assertEquals(errorMessage.getCode(), "1404", "Wrong error code received in message body for MSGTYPEID Header Missing");

            Assert.assertEquals(errorMessage.getUser_message(), expectedError, "Error Message in Error response body is wrong for Missing" + header);
        } else if (header.equalsIgnoreCase("DATAPROVIDER")) {
            logger.info("Validating DATAPROVIDER header missing scenario");
            Assert.assertEquals(errorMessage.getCode(), "1404", "Wrong error code received in message body for DATAPROVIDER Header Missing");

            Assert.assertEquals(errorMessage.getUser_message(), expectedError, "Error Message in Error response body is wrong for Missing" + header);
        } else if (header.equalsIgnoreCase("uri")) {
            logger.info("Validating client header missing scenario");
            Assert.assertEquals(errorMessage.getCode(), "1410", "Wrong error code received in message body for Client Header Missing");

            Assert.assertEquals(errorMessage.getUser_message(), expectedError, "Error Message in Error response body is wrong for Missing" + header);
            Assert.assertEquals(errorMessage.getDeveloper_message(), expectedError, "Developer Message in Error response body is wrong for Missing" + header);
        }
        logger.trace("Test Passed!");
    }


    public static void validateErrorResponseForNullScenario(ErrorMessage errorMessage, String expectedError, String header) {
        logger.trace("-----Validating the Error Message response for NULL SCENARIO ------!");
        logger.trace("---Validating the error code----- ");

        if (header.equalsIgnoreCase("ClientID")) {
            logger.info("Validating client header NULL scenario");
            Assert.assertEquals(errorMessage.getCode(), "1404", "Wrong error code received in message body for Client Header NULL");

            Assert.assertEquals(errorMessage.getUser_message(), expectedError, "Error Message in Error response body is wrong for NULL " + header);

        } else if (header.equalsIgnoreCase("MSGTYPEID")) {
            logger.info("Validating MSGTYPEID header NULL scenario");
            Assert.assertEquals(errorMessage.getCode(), "1430", "Wrong error code received in message body for MSGTYPEID Header NULL");

            Assert.assertEquals(errorMessage.getUser_message(), expectedError, "Error Message in Error response body is wrong for NULL" + header);
            Assert.assertEquals(errorMessage.getDeveloper_message(), "Invalid serialized unsecured/JWS/JWE object: Missing part delimiters", "Failed in Null MSGTYPE Header in Dev Message");

        } else if (header.equalsIgnoreCase("DATAPROVIDER")) {
            logger.info("Validating DATAPROVIDER header NULL scenario");
            Assert.assertEquals(errorMessage.getCode(), "1420", "Wrong error code received in message body for DATAPROVIDER Header NULL");

            Assert.assertEquals(errorMessage.getUser_message(), expectedError, "Error Message in Error response body is wrong for Null header" + header);

        } else if (header.equalsIgnoreCase("uri")) {
            logger.info("Validating client header NULL scenario");
            Assert.assertEquals(errorMessage.getCode(), "1410", "Wrong error code received in message body for Client Header NULL");

            Assert.assertEquals(errorMessage.getUser_message(), expectedError, "Error Message in Error response body is wrong for NULL" + header);
            Assert.assertEquals(errorMessage.getDeveloper_message(), expectedError, "Developer Message in Error response body is wrong for NULL" + header);
        }
        logger.trace("Test Passed!");
    }

    public static void validateResponseForNegativeScenario(ErrorMessage errorMessageCall, String errorMessageExpected, String errorCode) {
        logger.trace("Validating the Error Response for Negative Scenario");

        logger.trace("Validating the error Message in response body");
        Assert.assertEquals(errorMessageCall.getUser_message(), errorMessageExpected, "Error Message in Response Body is wrong");
        logger.trace("error Message in response body validation Done!");

        logger.trace("Validating the Auth Code");
        Assert.assertEquals(errorMessageCall.getCode(), errorCode, "Error Code in Message Body is Wrong, Expected :   " + errorCode + "   Actual Recived:   " + errorMessageCall.getCode());
        logger.trace("Validating the Auth Code Done!");
        logger.trace("Test Passed!");
    }

}
