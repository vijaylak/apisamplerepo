package com.sample.variables;

/**
 * Created by Vijay Lakshmikanthan on 1/30/2018.
 * List of constant Environment variables
 * The url and proxy informations have been removed
 * Username and Pswd are set as environment variables
 */
public class EnvironmentVariables {


    private static final String userName = System.getProperty("userName");
    private static final String password = System.getProperty("password");


    private static final String BASE_URL = "";

    private static final String BASE_MOCK_URL = "";

    private static final String PRE_INFO = "prefill_info/";

    private static final String ENCRY_PRE_INFO = "/encrypted_prefill_info";

    private static final String tokenURL = "";

    private static final String proxy = "proxy.sample.com";

    private static final int portNumber = 8080;

    public static String getUserName() {
        return userName;
    }

    public static String getPassword() {
        return password;
    }

    public static String getBaseUrl() {
        return BASE_URL;
    }

    public static String getBaseMockUrl() {
        return BASE_MOCK_URL;
    }

    public static String getPreInfo() {
        return PRE_INFO;
    }

    public static String getEncryPreInfo() {
        return ENCRY_PRE_INFO;
    }

    public static String getTokenURL() {
        return tokenURL;
    }

    public static String getProxy() {
        return proxy;
    }

    public static int getPortNumber() {
        return portNumber;
    }
}
