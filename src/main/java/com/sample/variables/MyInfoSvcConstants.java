package com.sample.variables;



/**
 * Created by Vijay Lakshmikanthan on 1/31/2018.
 * List of service related constant variables
 * The urls have been removed to protect sensitive information
 */
public class MyInfoSvcConstants {

    public static String MyInfoE1URL = "";
    public static String MyInfoE2URL = "";
    //useful constants
    public static final String JSON = "application/json";
    public static final String R_ID="request_id";
    public static final String C_ID="client_id";
    public static final String MSG_ID="message_type_id";
    public static final String D_Provider="data_provider";
    public static final String redirect_uri="redirect_uri";

    //Values (Person)
    public static final String ClientId="AA382697D085A1115764A1DF4530DC07";
    public static final String mType="3001";
    public static final String dataP="myInfo";

}
