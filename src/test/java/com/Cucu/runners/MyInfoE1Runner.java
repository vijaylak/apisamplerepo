package com.Cucu.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;

/**
 * Created by schag2 on 8/3/2017.
 */
@CucumberOptions(features = "src/test/resources/features/MyInfoE1.feature",
        format = {"json:oneClickService/reports/cucumber.json",
                "html:oneClickService/reports/site/cucumber-pretty","pretty",
                "json:target/cucumber.json"},glue = {"com.Cucu"},strict = false,tags = {"@E1Service"})
public class MyInfoE1Runner extends AbstractTestNGCucumberTests {
}
