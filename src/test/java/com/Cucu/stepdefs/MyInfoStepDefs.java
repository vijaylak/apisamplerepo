package com.Cucu.stepdefs;


import com.sample.helpers.RestPostHelper;
import com.sample.helpers.RestResponseValidator;
import com.sample.utils.JsonParser;
import com.jayway.restassured.response.Response;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.log4j.Logger;
import org.fluttercode.datafactory.impl.DataFactory;
import org.testng.Assert;

/**
 *
 */
public class MyInfoStepDefs {
    private static final Logger LOGGER = Logger.getLogger("StoLog");
    DataFactory dataFactory = new DataFactory();
    Response pullApiGetResp;
    PrefillSuccessResponse prefillResponseFromFile;
    PrefillSuccessResponse responseFromCall;
    ErrorMessage errorMessage;
    String httpResponse;


    @When("^user hits the GPS url with a GET call with '(.*)' in (.*)$")
    public void user_hits_the_GPS_url_with_a_GET_call_with_AuthID(String AuthID, String Environment) throws Throwable {
        LOGGER.trace("Testing in Environment:   " + Environment);
        LOGGER.trace("User hits the GPS url with Auth ID:   " + AuthID);
        httpResponse = RestPostHelper.getPrefillMyInfoData(AuthID, Environment);
    }

    @Then("^user receives a successful GPS response back (.*)$")
    public void user_receives_a_successful_GPS_response_back(String authID) throws Throwable {
        LOGGER.trace("user receives a successful GPS response back!");
        //Convert the response to value object
        prefillResponseFromFile = JsonParser.convertFileToResponseObject(authID);
        LOGGER.trace("-----Printing the object fom File----");
        LOGGER.trace("object fom File----" + JsonParser.printValueObject(prefillResponseFromFile));

        responseFromCall = JsonParser.convertHttpResponseToPrefill(httpResponse);

        LOGGER.trace("-----Printing Object From GET Call ----");
        LOGGER.trace("Printing Object From GET Call ----" + JsonParser.printValueObject(responseFromCall));

        //Validate using Json assert
        Assert.assertEquals(JsonParser.ObjectToString(prefillResponseFromFile), httpResponse, "Object Mis Match!!!!");
        LOGGER.trace("Test Passed!");
    }

    @When("^user hits the GPS url with a GET call with missing '(.*)' in (.*)$")
    public void user_hits_the_GPS_url_with_a_GET_call_with_missing_header_in_E(String header, String env) throws Throwable {
        LOGGER.trace("Testing in Environment:   " + env);
        httpResponse = RestPostHelper.getPrefillFailureMessage(header, "S9912375C", env);
    }

    @Then("^user ensures the response returns the (.*) and (.*)$")
    public void user_ensures_the_response_returns_the_ErrorMessage(String errorMessage, String header) throws Throwable {
        LOGGER.trace("user ensures the response returns Error Message");
        //Convert String to Object
        ErrorMessage errorResponse = (ErrorMessage) JsonParser.StringToObject(httpResponse, ErrorMessage.class);

        //Validate the Response, error code and the error message
        RestResponseValidator.validateErrorResponse(errorResponse, errorMessage, header);
    }

    @When("^user hits the GPS url with a GET call with null '(.*)' in (.*)$")
    public void user_hits_the_GPS_url_with_a_GET_call_with_null_header_in_E(String header, String env) throws Throwable {
        LOGGER.trace("Testing in Environment:   " + env);
        httpResponse = RestPostHelper.getPrefillFailureNullMessage(header, "S9912375C", env);
    }

    @Then("^user ensures the response returns the (.*) for (.*) when null$")
    public void user_ensures_the_response_returns_the_ErrorMessageForNull(String errorMessage, String header) throws Throwable {
        LOGGER.trace("user ensures the response returns Error Message");
        //Convert String to Object
        ErrorMessage errorResponse = (ErrorMessage) JsonParser.StringToObject(httpResponse, ErrorMessage.class);

        //Validate the Response, error code and the error message
        RestResponseValidator.validateErrorResponseForNullScenario(errorResponse, errorMessage, header);
    }

    @When("^user hits the GPS URL with a GET call for negative scenario '(.*)' in (.*) for (.*)$")
    public void user_hits_the_GPS_URL_with_a_GET_call_for_negative_scenario_AuthCode(String authCode, String env, int httpcode) throws Throwable {
        LOGGER.trace("GPS Service Get Calls for Negative Scenarios in : " + env);
        httpResponse = RestPostHelper.getPrefillFailureNegativeScenarioCall(authCode, env, httpcode);
        LOGGER.trace("Call Success!");
    }

    @Then("^user receives GPS response with error message (.*) and (.*)$")
    public void user_receives_GPS_response_with_error_message_errorMessage(String errorMessage,String errorCode) throws Throwable {
        LOGGER.info("Validating the Response code Received from the GPS Service for Negative Scenario");
        //Convert to Value Object
        ErrorMessage errorResponse = (ErrorMessage) JsonParser.StringToObject(httpResponse, ErrorMessage.class);

        //Validator
        RestResponseValidator.validateResponseForNegativeScenario(errorResponse,errorMessage,errorCode);
    }

    @Then("^user ensures the response returns the (.*) for (.*) when null for sample$")
    public void user_ensures_the_response_returns_the_ErrorMessageForNullForSample(String errorMessage, String header) throws Throwable {
        LOGGER.trace("user ensures the response returns Error Message");
        //Convert String to Object
        ErrorMessage errorResponse = (ErrorMessage) JsonParser.StringToObject(httpResponse, ErrorMessage.class);

        //Validate the Response, error code and the error message
        RestResponseValidator.validateErrorResponseForNullScenario(errorResponse, errorMessage, header);
    }

    @When("^user hits the GPS url with a GET call with missing '(.*)' for sample (.*)$")
    public void user_hits_the_GPS_url_with_a_GET_call_with_missing_header_for_sample_E(String authCode,String env) throws Throwable {
        LOGGER.trace("Trial Sample");
        RestPostHelper.getPrefillMyInfoDataSuccess(authCode);
    }
}
