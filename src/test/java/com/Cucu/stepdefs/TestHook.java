package com.Cucu.stepdefs;

import org.apache.log4j.Logger;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.log4j.Logger;


/**
 * Created by 595029 on 2/4/2017.
 */

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.log4j.Logger;

/**
 * These Test Hooks enable the methods under @before and @After to run before any tests.
 * These are TestNG Hooks
 */
public class TestHook {

    private final Logger logger = Logger.getLogger("StoLog");

    @Before
    public void setUp() throws Exception {
        logger.info("&&&&+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++&&&&");
        System.out.println("Before");

    }

    @After
    public void tearDown() throws Exception {
        logger.info("\nExecution Complete");
        System.out.println("After!");
    }
    
}

