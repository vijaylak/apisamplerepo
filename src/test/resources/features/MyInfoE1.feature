@E1Service
Feature: To Test the Apply Service

  @SuccessResponse
  Scenario Outline: To validate the successful Apply Api Call
    When Ruser hits the GPS url with a GET call with '<AuthID>' in E1
    Then Ruser receives a successful GPS response back <AuthID>
  Examples: iterations
    | AuthID    |
    | S9812381D |
    | S9912369I |
    | S9912370B |
    | S9912374E |
    | S6005054F |
    | S9912366D |
    | G1612352N |
    | S9812379B |
    | S9812382B |
    | S9812385G |
    | S9912363Z |

  @MissingHeader
  Scenario Outline: To Validate the Missing headers
    When Ruser hits the GPS url with a GET call with missing '<header>' in E1
    Then Ruser ensures the response returns the <ErrorMessage> and <header>
  Examples: iterations
    | header       | ErrorMessage                              |
    | ClientID     | Client Id Header was not specified        |
    | MSGTYPEID    | Message Type Id Header was not specified  |
    | DATAPROVIDER | Data Provider in Header was not specified |
    | URI          | Callback Uri is not present in Request    |

  @NullScenario
  Scenario Outline: To validate Null Headers
    When Ruser hits the GPS url with a GET call with null '<nullHeader>' in E1
    Then Ruser ensures the response returns the <ErrorMessage> for <nullHeader> when null
  Examples: iterations

    | nullHeader   | ErrorMessage                                                                             |
    | ClientID     | Client Id is not in the acceptable client list                                           |
    | MSGTYPEID    | Security Exception :Invalid serialized unsecured/JWS/JWE object: Missing part delimiters |
    | DATAPROVIDER | Security Exception : Encrypted payload parsing error                                     |
    | URI          | Callback Uri is not present in Request                                                   |


  @Negative
  Scenario Outline: To Validate all the negative edge case scenarios
    When Ruser hits the GPS URL with a GET call for negative scenario '<AuthCode>' in E1 for <HttpCode>
    Then Ruser receives GPS response with error message <errorMessage> and <errorCode>
  Examples: iterations
    | AuthCode       | errorMessage                                             | HttpCode | errorCode |
    | ERRORCODE_1470 | 2400:AuthCode error - missing, invalid, expired, revoked | 400      | 1470      |
    | ERRORCODE_1471 | 2401:Unauthorized. Auth Code expired.                    | 401      | 1471      |
    | ERRORCODE_1472 | 2404:Not Found. Auth Code not found or invalid.          | 404      | 1472      |


 #Person Response Excluded
 #  | ERRORCODE_1570 | 2500:Prefill API - MyInfo Internal Server Error          | 500      | 1570      |
 #  | ERRORCODE_1573 | 4401:Unauthorized                                        | 500      | 1573      |
 #  | ERRORCODE_1572 | 4400:Bad Request Error                                   | 500      | 1572      |
 #  | ERRORCODE_1574 | 4403:Digital Service is invalid                          | 500      | 1574      |
 #  | ERRORCODE_1575 | 4404:UIN/FIN is invalid                                  | 500      | 1575      |
 #  | ERRORCODE_1576 | 4408:Request Timeout at MyInfo                           | 500      | 1576      |
 #  | ERRORCODE_1577 | 4410:Request Person MyInfo URI not available             | 500      | 1577      |
 #  | ERRORCODE_1578 | 4415:Media Type Not Supported from MyInfo                | 500      | 1578      |
 #  | ERRORCODE_1580 | 4500:Error with request parameters or headers            | 500      | 1580      |
 #  | ERRORCODE_1581 | 4504:Gateway Timeout Error                               | 500      | 1581      |
 #  | ERRORCODE_1583 | 5102:Error while Parsing Person payload                  | 500      | 1583      |
 #  | ERRORCODE_1584 | 5103:Error while Parsing JWS Person                      | 500      | 1584      |
 #  | ERRORCODE_1585 | 5104:Unable to get parsed JSON from successful response  | 500      | 1585      |
 #  | ERRORCODE_1571 | 1571:Myinfo Error                                        | 500      | 1571      |
