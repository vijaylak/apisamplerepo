### Project Details
This test suit is used to Validate the GPS service from Apply.

### Background:
Apply Makes GET calls to GPS. GPS in turn makes calls to Person and Token APIs.


### Scope Of Testing:
Responses of GPS services are mocked using LISA. User hits the service between EApply and GPS using GET/POST calls.
Since the application connects to the GPS services, testing the application developed by mocking certain responses on the GPS side ensures comprehensive coverage of Edge cases
The organization repo does not contain libraries for REST-ASSURED at the time of development. Hence a standard HttpClient was added to make the API Calls.
The Framework was updated later to make calls using REST-ASSURED once the libraries became available.


### Testing Framework:
Cucumber, RestAssured/HttpClient,TestNG Asserts


### Jenkins options:

E1: -Dcucumber.options="--tags @E1Service" -Dmaven.test.failure.ignore=true

E2: -Dcucumber.options="--tags @E2Service" -Dmaven.test.failure.ignore=true

### Reports:
target/reports/cucumber-report-html

## Installation Instructions:

### Requirements: JDK 1.6+ , Maven
### Installation:
```Bash
Mvn install
```

### Framework Details and Flow:

This is a classic Cucumber driven framework. User starts by writing the feature files based on the Requirements provided.
Map the Feature file steps to methods in the step-definitions.

### Beans:
Use the Java bean classes to build the request and response of the corresponding API call.
This can either be taken from the backend code or user can use a variety of online jars to generate bean classes from JSON.
This module is empty and users can add their own Beans.

### Helpers:
-->RestPostHelper : helps the user to make REST(GET/POST) calls by passing a JSON/REQUEST object

-->RestResponseValidator: This class is used to validate the responses received after the service call. user needs to pass the response object to the methods in this class

-->RestRequestBuilder : This component is missing in this package as the testing relies on posting standard requests.

### Utils:
JsonParser: Is used to convert the responses (received after the service calls) to Json/Actual response objects based on response beans. This util is necessary for Thorough assertion.

InputJson: This folder contains the request Json that needs to be posted. Input data is maintained in this way as the requests posted are standard and do not change.









        
        
